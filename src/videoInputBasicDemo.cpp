// videoInputBasicDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#define EXTERN extern "C" __declspec(dllexport)

using namespace std;
triangleApp TAPP;

#pragma region DebugToUnity
typedef void(*FuncPtr)(const char *);
FuncPtr Debug;

EXTERN void SetDebugFunction(FuncPtr fp) {
	Debug = fp;
}
#pragma endregion


enum class byte : uint8_t {};

videoInput VI;
unsigned char * frame;


EXTERN void InitCameraVI(int index)
{
	int dev = VI.listDevices();
	/*VI.setupDevice(0, 640, 480, VI_COMPOSITE);*/
	VI.setupDevice(0, 640, 480);
	VI.setIdealFramerate(0, 25);
	int size = VI.getSize(0);
	frame = new unsigned char[size];
}

EXTERN void DisposeCameraVI() {
	VI.stopDevice(0);

}

EXTERN unsigned char* GetTextureVI(int* _size) {
	
	//retorna unsigned char*
	if (VI.isFrameNew(0))
	{
		//we get the pixels by passing in out buffer which gets filled
		//VI.getPixels(0, frame, true);
		VI.getPixels(0, frame, true,true);

		*_size = VI.getSize(0);
				
		return frame;

	}
	return nullptr;



}

EXTERN void GetTexture2VI(void** presult, int& _size) {

	//retorna unsigned char*
	if (VI.isFrameNew(0))
	{
		//we get the pixels by passing in out buffer which gets filled
		//VI.getPixels(0, frame, true);
		VI.getPixels(0, frame, true, true);

		_size = VI.getSize(0);

		Debug(to_string(_size).c_str());
		//return frame;
		*presult = frame;

	}
	*presult =  nullptr;



}


EXTERN int GetTexture1VI(void** pResult, int* Width, int* Height) {


	if (VI.isFrameNew(0))
	{
		//we get the pixels by passing in out buffer which gets filled
		//VI.getPixels(0, frame, true);
		VI.getPixels(0, frame, true, true);
		
		*Width  = VI.getWidth(0);
		*Height = VI.getHeight(0);
		*pResult = frame;
		return 0;

	}
	return -1;

}



